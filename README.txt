=====
Block Class
http://drupal.org/project/block_class
-----
Block Class was developed and is maintained by Four Kitchens
<http://fourkitchens.com>.


=====
Installation
-----

1. Enable the module
2. Visit the block configuration page at Administration -> Structure -> Block Layout
and click on the Configure link for a block.
3. Enter the classes in the field provided and save the block.